package Framework.Classes;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Dateps {
	
	private int day;
	private int month;
	private int year;
	private String date;

	public Dateps(String sedates) {
		String[] SA = null;
		
		SA = sedates.split("/");
		
		this.day = Integer.parseInt(SA[0]);
		this.month = Integer.parseInt(SA[1]);
		this.year = Integer.parseInt(SA[2]);
		this.date = sedates;	 
	}
	
	public Dateps (int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
		this.date = day + "/" + month + "/" + year;
		
	}

	public boolean validateDate() {
        boolean result = true;
        GregorianCalendar date = new GregorianCalendar();
        int days_month[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		
        if ((this.month < 1) || (this.month > 12)) 
            result = false;
        
        if (result) {
            date.set(this.year, this.month, this.day);
            
            if (date.isLeapYear(this.year)) 
            	days_month[2] = 29;
            
            if ((this.day < 1) || (this.day > days_month[this.month])) 
                result = false;
        }
        return result;
    }
	
	public boolean Change_data(int age, int antiquiti) {
		boolean switx = false;
		int rest = 0;
		
		rest = age - antiquiti;
		if(rest < 22)
			switx = false;
		else {
			switx  = true;
		}
		return switx;
	}
	
	public Calendar StringToCalendar(String date) {
		Calendar cadDate = new GregorianCalendar();
		Date Date = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			Date = format.parse(this.date);
			cadDate.setTime(Date);
		} catch (Exception e) {
			return cadDate;
		}
		return cadDate;
	}	

	public int ComparateDate(Dateps date) { 
		int var = 0;
		Calendar date0 = this.StringToCalendar("xx/xx/xxxx");
		Calendar date1 = date.StringToCalendar("xx/xx/xxxx");

		if (date0.before(date1))
			var = -1;
		else if(date0.after(date1))
			var = 1;
		else if(date0.equals(date1))
			var = 0;
		return var;
	}
	
	public int calculateAge(){
		int myage = 0;
		Calendar today = Calendar.getInstance();
		int dayz = today.get(Calendar.DATE); 
		int monthz = today.get(Calendar.MONTH)+1; 
	   	int yearz = today.get(Calendar.YEAR); 
	   	myage = yearz - this.year;
	   	
		if(monthz < this.month){
			myage = myage - 1;
		}else if(monthz == this.month){
			if(dayz < this.year){
			myage = myage - 1;
			}	
		}
	   	return myage;   	
	}
	
	public int RestTwoDates(Dateps d2) {
			
		Calendar d = this.StringToCalendar(date);
		Calendar dateToCal = d2.StringToCalendar(d2.toString());
		
		return (d.get(Calendar.YEAR) - dateToCal.get(Calendar.YEAR));
	}
	
	public int RestSystemDate() {
		Calendar dated=Calendar.getInstance();
		Calendar dates=this.StringToCalendar(date);
			
		return (dated.get(Calendar.YEAR) - dates.get(Calendar.YEAR));
			
	}
	
    public String toString() {  
        return this.day + "/" + this.month + "/" + this.year;  
     }
}
