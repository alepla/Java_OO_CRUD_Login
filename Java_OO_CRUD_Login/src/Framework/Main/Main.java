package Framework.Main;

import java.util.ArrayList;
import javax.swing.JOptionPane;
import Framework.Modules.User.Classes.Associate;
import Framework.Modules.User.Classes.Paralegal;
import Framework.Modules.User.Classes.Partner;
import Framework.Modules.User.Classes.Singleton.Singleton;
import Framework.Utils.*;
import Framework.Modules.User.Utils.CRUD.*;

/*
 * @author Alejandro Pla Cambra 1DAW
 */
public class Main {
	
	public static void main(String[] args) {
		String [] type = {"Partner", "Associate", "Paralegal", "Exit"};
		String [] type2 = {"Create", "Read", "Update", "Delete" ,"Order", "Exit"};
		String [] type3 = {"Read", "Update","Order", "Exit"};
		String [] type4 = {"Create", "Read", "Order", "Exit"};
		int lawyer = 0, partner = 0, associate = 0, paralegal = 0;
		boolean switx = true, switx2 = true;
		int login = 0;
		Partner s = null;
		Associate a = null;
		Paralegal p = null;
		Singleton.userPartner = new ArrayList <Partner> ();
		Singleton.userAssociate = new ArrayList <Associate> ();
		Singleton.userParalegal = new ArrayList <Paralegal> ();
		
		Dumies.BuildPartner();
		Dumies.BuildAssociate();
		Dumies.BuildParalegal();
		
		do {
			lawyer = Menu.menuBotons(type, "What would you like to do?", "Main");
				
			if(lawyer == 0) {
				//PARTNER
				login = Login.Loginl();
					if(login == 0) {
						do {
							partner = Menu.menuBotons(type2, "What would you like to do?", "Partner");
							switch(partner) {
							//CREATE
							case 0:
								Create.CreatePartner(s);
								break;
							//READ
							case 1: 
								Read.ReadPartner();
								break;
							//UPDATE
							case 2: 
								Update.UpdatePartner();
								break;
							//DELETE 
							case 3:
								Delete.DeletePartner();
								break;
							//ORDER
							case 4:
								Order.ChancheOrderPartner();
								break;
							//EXIT

							default: 
								JOptionPane.showMessageDialog(null, "Exiting of the aplication", "Exiting",JOptionPane.INFORMATION_MESSAGE);
								System.exit(0);
								switx = false;
					}
						}while((switx == true));
					}
					else if(login == 1){
						do {
							partner = Menu.menuBotons(type3, "What would you like to do?", "Partner");
							switch(partner) {
									//READ
								case 0: 
									Read.ReadPartner();
									break;
									//UPDATE
								case 1: 
									Update.UpdatePartner();
								break;
									//ORDER
								case 2:
									Order.ChancheOrderPartner();
									break;
									//EXIT
								default: 
									JOptionPane.showMessageDialog(null, "Exiting of the aplication", "Exiting",JOptionPane.INFORMATION_MESSAGE);
									System.exit(0);
									switx = false;
							}
						}while((switx == true));
					}
					else if(login == 2){
						do {
							partner = Menu.menuBotons(type4, "What would you like to do?", "Partner");
							switch(partner) {
									//CREATE
								case 0:
										Create.CreatePartner(s);
									break;
									//READ
								case 1: 
										Read.ReadPartner();
									break;
									//ORDER
								case 2:
										Order.ChancheOrderPartner();
									break;
									//EXIT
								default: 
									JOptionPane.showMessageDialog(null, "Exiting of the aplication", "Exiting",JOptionPane.INFORMATION_MESSAGE);
									System.exit(0);
									switx = false;
							}
						}while((switx == true));
					}
				
			}
			//ASSOCIATE
			if(lawyer == 1) {
				//PARTNER
				login = Login.Loginl();
					if(login == 0) {
						do {
							associate = Menu.menuBotons(type2, "What would you like to do?", "Associate");
							switch(associate) {
							//CREATE
						case 0:
								Create.CreateAssociate(a);
							break;
							//READ
						case 1: 
								Read.ReadAssociate();
							break;
							//UPDATE
						case 2: 
								Update.UpdateAssociate();
							break;
							//DELETE 
						case 3:
								Delete.DeleteAssociate();
							break;
							//ORDER
						case 4:
								Order.ChancheOrderAssociate();
							break;
							//EXIT
						default: 
							JOptionPane.showMessageDialog(null, "Exiting of the aplication", "Exiting",JOptionPane.INFORMATION_MESSAGE);
							System.exit(0);
							switx = false;
					}
						}while((switx == true));
					}
					else if(login == 1){
						do {
							associate = Menu.menuBotons(type3, "What would you like to do?", "Associate");
							switch(associate) {
									//READ
								case 0: 
										Read.ReadAssociate();
									break;
									//UPDATE
								case 1: 
									Update.UpdateAssociate();
								break;
									//ORDER
								case 2:
										Order.ChancheOrderAssociate();
									break;
									//EXIT
								default: 
									JOptionPane.showMessageDialog(null, "Exiting of the aplication", "Exiting",JOptionPane.INFORMATION_MESSAGE);
									System.exit(0);
									switx = false;
							}
						}while((switx == true));
					}
					else if(login == 2){
						do {
							associate = Menu.menuBotons(type4, "What would you like to do?", "Associate");
							switch(associate) {
									//CREATE
								case 0:
										Create.CreateAssociate(a);
									break;
									//READ
								case 1: 
										Read.ReadAssociate();
									break;
									//ORDER
								case 2:
										Order.ChancheOrderAssociate();
									break;
									//EXIT
								default: 
									JOptionPane.showMessageDialog(null, "Exiting of the aplication", "Exiting",JOptionPane.INFORMATION_MESSAGE);
									System.exit(0);
									switx = false;
							}
						}while((switx == true));
					}
				
			}
			//END-ASSOCIATE
			//PARLAEGAL
			else if(lawyer == 2) {
				login = Login.Loginl();
				if(login == 0) {
					do {
						paralegal = Menu.menuBotons(type2, "What would you like to do?", "Associate");
						switch(paralegal) {
						//CREATE
					case 0:
							Create.CreateParalegal(p);
						break;
						//READ
					case 1: 
							Read.ReadParalegal();
						break;
						//UPDATE
					case 2: 
							Update.UpdateParalegal();
						break;
						//DELETE 
					case 3:
							Delete.DeleteParalegal();
						break;
						//ORDER
					case 4:
							Order.ChancheOrderParalegal();
						break;
						//EXIT
					default: 
						JOptionPane.showMessageDialog(null, "Exiting of the aplication", "Exiting",JOptionPane.INFORMATION_MESSAGE);
						System.exit(0);
						switx = false;
				}
					}while((switx == true));
				}
				else if(login == 1){
					do {
						paralegal = Menu.menuBotons(type3, "What would you like to do?", "Paralegal");
						switch(paralegal) {
								//READ
							case 0: 
									Read.ReadParalegal();
								break;
								//UPDATE
							case 1: 
								Update.UpdateParalegal();
							break;
								//ORDER
							case 2:
									Order.ChancheOrderParalegal();
								break;
								//EXIT
							default: 
								JOptionPane.showMessageDialog(null, "Exiting of the aplication", "Exiting",JOptionPane.INFORMATION_MESSAGE);
								System.exit(0);
								switx = false;
						}
					}while((switx == true));
				}
				else if(login == 2){
					do {
						paralegal = Menu.menuBotons(type4, "What would you like to do?", "Paralegal");
						switch(paralegal) {
								//CREATE
							case 0:
									Create.CreateParalegal(p);
								break;
								//READ
							case 1: 
									Read.ReadParalegal();
								break;
							case 2:
								//ORDER
									Order.ChancheOrderParalegal();
								break;
								//EXIT
							default: 
								JOptionPane.showMessageDialog(null, "Exiting of the aplication", "Exiting",JOptionPane.INFORMATION_MESSAGE);
								System.exit(0);
								switx = false;
						}
					}while((switx2 == true));
				}
				//END-PARALEGAL
			}
			else {
			 	JOptionPane.showMessageDialog(null, "Exiting of the aplication", "Exiting",JOptionPane.INFORMATION_MESSAGE);
				System.exit(0);
			}
		}while (lawyer < 3);
	}
}