package Framework.Modules.User.Classes;

import Framework.Classes.*;

public class Associate extends Lawyer {

	//Porpiedades
	
	private String average_grade;
	private Dateps years_working;
	private int years;
	private int win;
	private int lost;
	private int bonuses;

	
	//Constructor
	
	public Associate(String name, String lastName, String dni,String tlf, Dateps birthDate, String average_grade,
					String gender, Dateps years_working, int win, int lost, int bonuses) {

		super(name, lastName, dni, tlf, birthDate, gender);
		this.years_working = years_working;
		this.average_grade = average_grade;
		this.win = win;
		this.lost = lost;
		this.bonuses = bonuses;
		this.setYears(calculate_years_working());
		this.calculate_ncases();

	}
	
	public Associate(){
		super();
	}
	public Associate(String dni) {
		super(dni);
	}
	
	public int calculate_ncases() {
		return this.win + this.lost;
	}
	public int getYears() {
		return years = calculate_years_working();
	}
	public void setYears(int years) {
		this.years = this.calculate_years_working();
	}
	public int calculate_years_working() {
		return years = years_working.calculateAge();
	}
	
	//Metodos-getter&setters

	public String getAverage_grade() {
		return average_grade;
	}
	public void setAverage_grade(String average_grade) {
		this.average_grade = average_grade;
	}
	
	public int getWin() {
		return win;
	}
	public void setWin(int win) {
		this.win = win;
		this.calculate_ncases();
	}
	
	public int getLost() {
		return lost;
	}
	public void setLost(int lost) {
		this.lost = lost;
		this.calculate_ncases();
	}
	
	public int getBonuses() {
		return bonuses;
	}
	public void setBonuses(int bonuses) {
		this.bonuses = bonuses;
	}
	public void setYears_working(Dateps years_working) {
		this.years_working = years_working;
		this.setYears(calculate_years_working());
	}
	
	// to_String
	public String toString() {
		String cad = "";
					
		cad = cad + ("Name: " + this.getName() + "\n" );
		cad = cad + ("Last name: " + this.getLastName() + "\n" );
		cad = cad + ("DNI: " + this.getDni() + "\n");
		cad = cad + ("Telephone: "+ this.getTlf()+"\n");
		cad = cad + ("Birth date: " + this.getBirthDate() +"\n");
		cad = cad + ("Gender: " + this.getGender() + "\n"); 
		cad = cad + ("Age: " + this.calculate_age() + "\n");
		cad = cad + ("Years in the firm: " + this.calculate_years_working() + "\n");
		cad = cad + ("Average grade: " + this.getAverage_grade() + "\n");
		cad = cad + ("Number of cases: " + this.calculate_ncases()  + " \n"); 
		cad = cad + ("Number of won cases: " + this.getWin() + " \n");
		cad = cad + ("Number of lost cases: "+ this.getLost() + "\n");
		cad = cad + ("Bonuses: "+ this.getBonuses() + "\n");
		return cad ;
		}
}
