package Framework.Modules.User.Classes;

import java.io.Serializable;

import Framework.Classes.Dateps;

public abstract class Lawyer implements Comparable<Lawyer>, Serializable{
	
	//Propiedades
	private String name;
	private String lastName;
	private String dni;
	private String tlf;
	private Dateps birthDate;
	private int age;
	private String gender;
	
	//Constructor
	public Lawyer (String name, String lastName, String dni,String tlf, Dateps birthDate, String gender) {
		this.name = name;
		this.lastName = lastName;
		this.dni = dni;
		this.tlf = tlf;
		this.birthDate = birthDate;
		this.gender = gender;
		this.setAge(calculate_age());
	}
	
	public Lawyer() {
	}
	
	public Lawyer(String dni) {
		this.dni = dni;
	}
	
	public int getAge() {
		return age = calculate_age();
	}
	public void setAge(int age) {
		this.age = this.calculate_age();
	}
	public int calculate_age() {
		return age = birthDate.calculateAge();
	}
	
	//Metodos-getter&setters
	
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return this.name;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getLastName() {
		return this.lastName;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getDni() {
		return this.dni;
	}
	public void setTlf(String tlf) {
		this.tlf = tlf;
	}
	public String getTlf() {
		return this.tlf;
	}
	public void setBirthDate(Dateps birthDate) {
		this.birthDate = birthDate;
		this.setAge(calculate_age());
	}
	public Dateps getBirthDate() {
		return this.birthDate;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getGender() {
		return this.gender;
	}
		
	public int compareTo(Lawyer param) {
		if(this.getDni().compareTo(param.getDni())>0)
			return 1;
		if(this.getDni().compareTo(param.getDni())<0)
			return -1;
		return 0;
	  }
	
	public boolean equals(Object param){
		return getDni().equals(((Lawyer)param).getDni());
	}
	
	// to_String
	public abstract String toString();
	
}