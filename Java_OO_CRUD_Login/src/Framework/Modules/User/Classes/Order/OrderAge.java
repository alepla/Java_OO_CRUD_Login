package Framework.Modules.User.Classes.Order;

import java.util.Comparator;
import Framework.Modules.User.Classes.Lawyer;

public class OrderAge implements Comparator <Lawyer>{

	public int compare (Lawyer L1, Lawyer L2) {
		if(L1.getAge() > L2.getAge())
			return 1;
		
		if(L1.getAge() < L2.getAge())
			return -1;
		return 0;
	}
}
