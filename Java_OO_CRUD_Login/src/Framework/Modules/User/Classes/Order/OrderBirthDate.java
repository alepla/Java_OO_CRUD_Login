package Framework.Modules.User.Classes.Order;

import java.util.Comparator;
import Framework.Modules.User.Classes.Lawyer;

public class OrderBirthDate implements Comparator <Lawyer>{

	public int compare (Lawyer L1, Lawyer L2) {
		return L1.getBirthDate().ComparateDate(L2.getBirthDate());
	}
}