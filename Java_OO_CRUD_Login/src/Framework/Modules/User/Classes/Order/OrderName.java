package Framework.Modules.User.Classes.Order;

import java.util.Comparator;
import Framework.Modules.User.Classes.Lawyer;

public class OrderName implements Comparator <Lawyer>{

	public int compare (Lawyer L1, Lawyer L2) {
		if(L1.getName().compareTo(L2.getName())>0)
			return 1;
		
		if(L1.getName().compareTo(L2.getName())<0)
			return -1;
		
		return 0;
	}
}
