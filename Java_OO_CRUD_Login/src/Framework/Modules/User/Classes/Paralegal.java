package Framework.Modules.User.Classes;

import Framework.Classes.*;

public class Paralegal extends Lawyer {
	//Propiedades
	
	private String studies;
	private int contract;
	private Dateps permanence_contract;
	private String email;
	private String passwd;
		
	//Constructor
		
	public Paralegal (String name, String lastName, String dni,String tlf, Dateps birthDate, String gender,
  				    String studies,Dateps permanence_contract, String email, String passwd) {
		
		super(name, lastName, dni, tlf, birthDate, gender);
		this.studies = studies;
		this.permanence_contract = permanence_contract;
		this.email = email;
		this.passwd = passwd;
		this.setContract(calculate_permanence_contract());
	}
	
	public Paralegal(){
		super();
	}
	public Paralegal(String dni) {
		super(dni);
	}
	
	public int getContract() {
		return contract = calculate_permanence_contract();
	}
	public void setContract(int years) {
		this.contract = this.calculate_permanence_contract();
	}
	public int calculate_permanence_contract() {
		return contract = permanence_contract.calculateAge();
	}
	//Metodos-getter&setters
		
	public String getStudies() {
		return studies;
	}
		
	public void setStudies(String studies) {
		this.studies = studies;
	}
		
	public String getEmail() {
		return email;
	}
		
	public void setEmail(String email) {
		this.email = email;
	}
		
	public String getPasswd() {
		return passwd;
	}
		
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	
	public void setPermanence_contract(Dateps permanence_contract) {
		this.permanence_contract = permanence_contract;
		this.setContract(calculate_permanence_contract());
	}
		
	// to_String
	public String toString() {
		String cad = "";
				
		cad = cad + ("Name: " + this.getName() + "\n" );
		cad = cad + ("Last name: " + this.getLastName() + "\n" );
		cad = cad + ("DNI: " + this.getDni() + "\n");
		cad = cad + ("Telephone: "+ this.getTlf()+"\n");
		cad = cad + ("Birth date: " + this.getBirthDate() +"\n");
		cad = cad + ("Gender: " + this.getGender() + "\n");
		cad = cad + ("Age: " + this.calculate_age() + "\n");
		cad = cad + ("University: " + this.getStudies() + "\n");
		cad = cad + ("Permanence: " + this.calculate_permanence_contract() + "\n");
		cad = cad + ("Email: " + this.getEmail()  + " \n"); 
		cad = cad + ("Password: " + this.getPasswd() + " \n");

		return cad ;
		}
}

