package Framework.Modules.User.Classes;

import Framework.Classes.*;

public class Partner extends Lawyer{
	
	//Propiedades
	
	private String email;
	private String passwd;
	private int salary;
	private int invoicing;
	
	//Constructor
	
	public Partner (String name, String lastName, String dni,String tlf, Dateps birthDate, String gender,
				    String email, String passwd,int salary, int invoicing) {
		
		super(name, lastName, dni, tlf, birthDate, gender);
		this.email = email;
		this.passwd = passwd;
		this.invoicing = invoicing;
		this.calculate_salary();
	}
	
	public Partner(){
		super();
	}
	public Partner(String dni) {
		super(dni);
	}
	
	public int calculate_salary() {
		if (this.invoicing >= 20000)
			this.salary = 10000;
		else if ((this.invoicing < 20000) && (this.invoicing > 1500))
			this.salary = 7000;
		else if (this.invoicing < 1500)
			this.salary = 5000;
		return this.salary;
		
	//Metodos-getter&setters

	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPasswd() {
		return passwd;
	}
	
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	
	public int getInvoicing() {
		return invoicing;
	}
	public void setInvoicing(int invoicing) {
		this.invoicing = invoicing;
		this.calculate_salary();
	}
	
	// to_String
	public String toString() {
		String cad = "";
				
		cad = cad + ("Name: " + this.getName() + "\n" );
		cad = cad + ("Last name: " + this.getLastName() + "\n" );
		cad = cad + ("DNI: " + this.getDni() + "\n");
		cad = cad + ("Telephone: "+ this.getTlf()+"\n");
		cad = cad + ("Birth date: " + this.getBirthDate() +"\n");
		cad = cad + ("Gender: " + this.getGender() + "\n"); 
		cad = cad + ("Age: " + this.calculate_age() + "\n");
		cad = cad + ("Email: " + this.getEmail()  + " \n"); 
		cad = cad + ("Password: " + this.getPasswd() + " \n");
		cad = cad + ("Salary: "+ this.calculate_salary()+ "\n");
		cad = cad + ("Invoicing: "+ this.getInvoicing()+ "\n");
		return cad ;
		}
}
