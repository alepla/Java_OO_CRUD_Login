package Framework.Modules.User.Utils.CRUD;

import Framework.Modules.User.Utils.Functions_lawyer;
import javax.swing.JOptionPane;

import Framework.Modules.User.Classes.Associate;
import Framework.Modules.User.Classes.Paralegal;
import Framework.Modules.User.Classes.Partner;
import Framework.Modules.User.Classes.Singleton.*;

public class Create {

	public static void CreatePartner(Partner P) {
		int search = -1;
		
		P = Functions_lawyer.RequestPartner();
		search = Find.FindPartner(P);
		if (search != -1) {
			JOptionPane.showMessageDialog(null, "Ya esta en uso ese dni");
		} 
		else {
			P = (Partner) Functions_lawyer.request_data(1);
			Singleton.userPartner.add(P);
		}
	}
	public static void CreateAssociate(Associate A) {
		int search = -1;
		
		A = Functions_lawyer.RequestAssociate();
		search = Find.FindAssociate(A);
		if(search != -1) {
			JOptionPane.showMessageDialog(null, "Ya esta en uso ese dni");
		}
		else {
			A = (Associate) Functions_lawyer.request_data(2);
			Singleton.userAssociate.add(A);
		}
	}
	public static void CreateParalegal(Paralegal PA) {
		int search = -1;
		
		PA = Functions_lawyer.RequestParalegal();
		search = Find.FindParalegal(PA);
		if(search != -1) {
			JOptionPane.showMessageDialog(null, "Ya esta en uso ese dni");
		}
		else {
			PA = (Paralegal) Functions_lawyer.request_data(3);
			Singleton.userParalegal.add(PA);
		}
	}
}
