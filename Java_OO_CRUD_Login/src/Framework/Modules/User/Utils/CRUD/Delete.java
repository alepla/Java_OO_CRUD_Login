package Framework.Modules.User.Utils.CRUD;

import Framework.Modules.User.Classes.Partner;
import Framework.Modules.User.Classes.Associate;
import Framework.Modules.User.Classes.Paralegal;
import Framework.Modules.User.Classes.Singleton.*;

import javax.swing.JOptionPane;

public class Delete {

	public static void DeletePartner() {
		int search = -1;
		Partner P = null;
		
		if(Singleton.userPartner.isEmpty()){
			JOptionPane.showMessageDialog(null, "Error the user is empty");
		}
		else {
			P = Find.IDPartner();
			if(P != null) {
				search = Find.FindPartner(P);
				if(search != -1) {
					Singleton.userPartner.remove(search);
					JOptionPane.showMessageDialog(null, "The user was deleted");
				}
				else {
					JOptionPane.showMessageDialog(null, "Error imposible delete the user");
				}
			}
		}
	}
	public static void DeleteAssociate() {
		int search = -1;
		Associate A = null;
		if(Singleton.userAssociate.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Error the user is empty");
		}
		else {
			A = Find.IDAssociate();
			if(A != null) {
				search = Find.FindAssociate(A);
				if(search != -1) {
					Singleton.userAssociate.remove(search);
					JOptionPane.showMessageDialog(null, "The user was deleted");
				}
				else {
					JOptionPane.showMessageDialog(null, "Error imposible delete the user");
				}
			}
		}
	}
	public static void DeleteParalegal() {
		int search = -1;
		Paralegal PA = null;
		
		if(Singleton.userParalegal.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Error the user is empty");
		}
		else {
			PA= Find.IDParalegal();
			if(PA != null) {
				search = Find.FindParalegal(PA);
				if(search != -1) {
					Singleton.userParalegal.remove(search);
					JOptionPane.showMessageDialog(null, "The user was deleted");
				}
				else {
					JOptionPane.showMessageDialog(null, "Error imposible delete the user");
				}
			}
		}
	}
}
