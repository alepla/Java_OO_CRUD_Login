package Framework.Modules.User.Utils.CRUD;

import Framework.Classes.Dateps;
import Framework.Modules.User.Classes.Associate;
import Framework.Modules.User.Classes.Paralegal;
import Framework.Modules.User.Classes.Partner;
import Framework.Modules.User.Classes.Singleton.Singleton;

public class Dumies {


	public static String DNI() {
		String dni = "";
		int numbers = (int) (Math.random() * (10000000 - 99999999) + 99999999);
		char [] letters = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'W'};
		
		int position = (int) (Math.random() * 180) % 22;
		return dni = Integer.toString(numbers) + letters[position];
	}
	
	public static String Name() {
		String Name = "";
		String[] Names = {"Alejandro", "Dani", "Adrian", "Oscar", "Alex", "Josep", "Carlos", "Jose", "Kevin",
	            "Manolo", "Ivan", "Paco", "Steve", "Bruce", "Miguel", "Jorge", "Sergi", "Clara", "Carla", "Valeria", "Altea", "Maria",
	            "Elena", "Ester", "Carolina", "Victoria","Carmen", "Lola", "Paula", "Lorena", "Andrea", "Yanira", "Aina", "Sonsoles"};
		
		int position = (int) (Math.random() * 34);
		//System.out.println(Arrays.toString(Names) + position);
		return Name = Names[position];	
	}
	
	public static String Gender(String Name) {
		String[] Names = {"Alejandro", "Dani", "Adrian", "Oscar", "Alex", "Josep", "Carlos", "Jose", "Kevin",
	            "Manolo", "Ivan", "Paco", "Steve", "Bruce", "Miguel", "Jorge", "Sergi", "Clara", "Carla", "Valeria", "Altea", "Maria",
	            "Elena", "Ester", "Carolina", "Victoria","Carmen", "Lola", "Paula", "Lorena", "Andrea", "Yanira", "Aina", "Sonsoles" };
		String[] Gender = {"Male", "Female"};
		String G = "";
		
		for(int i = 0; i < Names.length; i++) {
			if (Name.equals(Names[i])) {
				//System.out.println(Arrays.toString(Names));
				if(i <= 16) {
					G = Gender[0];
				}
				else if(i >= 17) {
					G = Gender[1];
				}
			}
		}
	return G;
	}
	
	public static String LastName() {
		String LastName = "";
		String [] LastNames = {"Pla", "Cambra", "Ruiz", "Ortiz", "Calabuig", "Mico", "Alcobendas", "Sanchis", "Soriano",
					"Auditore", "Zafra", "Selva", "Montagud", "Gramage", "Mu�oz", "Lizandra", "Revert", "Moreno", "Casanova",
					"Martinez", "Lopez", "Knightley", "Ruffalo", "Belda", "Mateu", "Ribera", "Reynolds", "Rubio", "Carano",
					"Fimmel", "Pit", "Leto", "Smith", "Delevigne"};

		int position = (int) (Math.random() * 34);
		return LastName = LastNames[position];
	}
	

	public static String Telephone() {
		int number = 0;
		
		int prefijo = (int) (Math.random() * (600 - 699) + 699);
		int sufijo = (int) ( Math.random() * (000000 - 999999) + 999999);
		String cad =(prefijo + "" + sufijo);
		
		return cad;
	}
	
	//END-GENERALS
	
	//BEGIN-DATEPS
	
	public static Dateps BirthDate() {
		int day = (int) (Math.random() * (1 - 31) + 31);
		int month = (int) (Math.random() * (1 - 12) + 12);
		int year = (int) (Math.random() * (1920 - 1995) + 1995);
		
		return new Dateps (day, month, year);
	}
	
	public static Dateps PermanenceContract() {
		int year = 1;
		int day = (int) (Math.random() * (1 - 31) + 31);
		int month = (int) (Math.random() * (1 - 12) + 12);
		int [] Year = {(int) (Math.random() * (1920 - 1999) + 1999), (int) (Math.random() * (2000 - 2017) + 2017)};
		
		int position = (int) (Math.random() * 180) % 2;
		year = Year[position];
		
		return new Dateps (day, month, year);
	}
	
	public static Dateps Years_workig() {
		
		int day = (int) (Math.random() * (1 - 31) + 31);
		int month = (int) (Math.random() * (1 - 12) + 12);
		int year = (int) (Math.random() * (2010 - 2017) + 2017);
			
		return new Dateps (day, month, year);
	}

	
	//END-DATEPS
	
	
	public static String Email() {
		String email = "";
		String[] emails = {"Aleja", "Rafa", "Manolo", "Steve", "Rosendo",
				"Sevill", "Vlc", "Carlos", "Smilf",
	            "Root", "Iphone", "Bomber", "Thor",
	            "Aida", "Marti", "Carla", "Xazmin", "Shaw", "MrRobot",
	            "Flash", "Outlander", "Viking", "GoT",
	            "Suits", "Stranger", "Things", "Arrow", "TWD", 
	            "Rick", "Morty", "Lord", "Wow", "Ventormenta", "Jeff"};
		
		int position = (int) (Math.random() * 180) % 22;
		return email = emails[position]+"@gmail.com";
	}
	
	public static String Password() {
		String pass = "";
		String[] passwords = {"123", "hasta1", "para23", "segun43", "sin543", "por53", "onetwo", "treefor", "sifive",
	            "sobre12", "2893", "6666", "passwpassw", "msi43", "ip127", "macmola", "samsung25", "pla32", "moreno56",
	            "aaawww", "aDf2f", "jkh2UG", "DdFi2", "hIb9", "452FFFff", "12345", "Stike12", "Daredevil2", "JessicaJ", "IronFist",
	            "LukeCage", "Punisher123", "Dagny5", "TeW6"};
		
		int position = (int) (Math.random() * 180) % 22;
		return pass = passwords[position];
	}
	
	public static int Invoicing() {
		int Invoicing = (int) (Math.random() * (4000 - 22000) + 22000);
		return (int) Math.rint(Invoicing * 100) / 100;
	}
	
	public static String AverageGrade(){
		String AV = "";
		String [] av = {"C", "B", "B+", "A", "A+", "A++"};
		
		int position = (int) (Math.random() * 222) % 6;
		return AV = av[position];
	}
	
	public static String Studies () {
		String studies = "";
		String [] studie = {"UPV", "UV", "Oxford", "Harvard", "Columbia", "Pringstone", "other"};
		
		int position = (int) (Math.random() * 222) % 7;
		return studies = studie[position];
	}
	
	
	public static int Bonuses() {
		int bonuses = (int) (Math.random() * (200 - 1000) + 1000);
		return (int) Math.rint(bonuses * 100) / 100;
	}
	
	
	public static int Win() {
		int Caseswin = 0;
		int position = (int) (Math.random() * 50) + 1;
		
		return Caseswin = position;
	}
	
	public static int Lost() {
		int Caselost = 0;
		int position = (int) (Math.random() * 50) + 1;
		
		return Caselost = position;
	}
	
	
	
	public static void BuildPartner() {
		for (int i = 0; i < 6; i++) {
			String Name = Name();
			Partner P = new Partner (Name, LastName(), DNI(), Telephone(), BirthDate(), Gender(Name), Email(), Password(), i, Invoicing());
			Singleton.userPartner.add(P);
		}
	}
	
	public static void BuildAssociate() {
		for (int i = 0; i < 6; i++) {
			String Name = Name();
			Associate A = new Associate (Name, LastName(), DNI(), Telephone(), BirthDate(), AverageGrade(), Gender(Name), Years_workig(), Win(), Lost(), Bonuses());
			Singleton.userAssociate.add(A);
		}
	}
	
	public static void BuildParalegal() {
		for (int i = 0; i < 6; i++) {
			String Name = Name();
			Paralegal PA = new Paralegal (Name, LastName(), DNI(), Telephone(), BirthDate(), Gender(Name),Studies(), PermanenceContract(), Email(), Password());
			Singleton.userParalegal.add(PA);
		}
	}

}
