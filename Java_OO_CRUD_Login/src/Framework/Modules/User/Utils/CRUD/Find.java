package Framework.Modules.User.Utils.CRUD;

import Framework.Modules.User.Classes.*;
import Framework.Modules.User.Classes.Singleton.*;
import Framework.Utils.Menu;

public class Find {
	public static int FindPartner(Partner partner) { 
		for (int i = 0; i <= (Singleton.userPartner.size()-1); i++){
			if((Singleton.userPartner.get(i)).equals(partner) )
				return i;
		}
		return -1;
	}
	
	public static int FindAssociate(Associate associate) { 
		for (int i = 0; i <= (Singleton.userAssociate.size()-1); i++){
			if((Singleton.userAssociate.get(i)).equals(associate) )
				return i;
		}
		return -1;
	}
	
	public static int FindParalegal(Paralegal paralegal) { 
		for (int i = 0; i <= (Singleton.userParalegal.size()-1); i++){
			if((Singleton.userParalegal.get(i)).equals(paralegal) )
				return i;
		}
		return -1;
	}
	
	//BEGIN-VECTORS
	
	public static String [] VectorPartner() {
		Partner P = null;
		String list = "";
		int AL = Singleton.userPartner.size();
		String [] part = new String[AL];
		
		for(int i = 0; i < AL; i++) {
			P = (Partner) Singleton.userPartner.get(i);
			list = P.getDni() + "-------" + P.getName() + " " + P.getLastName();
			part[i] = list;
		}
		return part;
	}
	
	public static String [] VectorAssociate() {
		Associate A = null;
		String list = "";
		int AL = Singleton.userAssociate.size();
		String [] aso = new String[AL];
		
		for(int i = 0; i < AL; i++) {
			A = (Associate) Singleton.userAssociate.get(i);
			list = A.getDni() + "-------" + A.getName() + " " + A.getLastName();
			aso[i] = list;
		}
		return aso;
	}
	
	public static String [] VectorParalegal() {
		Paralegal PA = null;
		String list = "";
		int AL = Singleton.userParalegal.size();
		String [] para = new String[AL];
		
		for(int i = 0; i < AL; i++) {
			PA = (Paralegal) Singleton.userParalegal.get(i);
			list = PA.getDni() + "-------" + PA.getName() + " " + PA.getLastName();
			para[i] = list;
		}
		return para;
	}
	
	//END-VECTORS
	
	//BEGIN-ID
	
	public static Partner IDPartner() {
		Partner P = null;
		String ID = "";
		String [] Partner = VectorPartner();
		String search = Menu.menuCombo(Partner, "All the users", "Lawyer");
		
		if(search != null) {
			for(int i = 0; i < 9; i++) {
				ID += search.charAt(i);
			}
			P = new Partner(ID);
		}
		return P;
	}
	
	public static Associate IDAssociate() {
		Associate A = null;
		String ID = "";
		String [] Associate = VectorAssociate();
		String search = Menu.menuCombo(Associate, "All the users", "Lawyer");
		
		if(search != null) {
			for(int i = 0; i < 9; i++) {
				ID += search.charAt(i);
			}
			A = new Associate(ID);
		}
		return A;
	}
	public static Paralegal IDParalegal() {
		Paralegal PA = null;
		String ID = "";
		String [] Paralegal = VectorParalegal();
		String search  = Menu.menuCombo(Paralegal, "All the users", "Lawyer");
		
		if(search != null) {
			for(int i = 0; i < 9; i++) {
				ID += search.charAt(i);
			}
			PA = new Paralegal(ID);
		}
		return PA;
	}
	//END-ID
}
