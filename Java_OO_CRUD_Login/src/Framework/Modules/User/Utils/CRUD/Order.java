package Framework.Modules.User.Utils.CRUD;

import java.util.Collections;
import javax.swing.JOptionPane;

import Framework.Modules.User.Classes.Order.*;
import Framework.Modules.User.Classes.Singleton.Singleton;
import Framework.Utils.Menu;

public class Order {
	public static void ChancheOrderPartner() {
		String [] type = { "DNI", "Name", "Age", "BirthDate"};
		String main = "";
		
		if(Singleton.userPartner.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Data is empty");
		}
		else {
			main = 	Menu.menuCombo(type, "How can you order the useres?", "Lawyer");
			if(main != null) {
				switch (main) {
				case "DNI":
					Collections.sort(Singleton.userPartner);
					break;
				case "Name":
					Collections.sort(Singleton.userPartner, new OrderName());
					break;
				case "Age":
					Collections.sort(Singleton.userPartner, new OrderAge());
					break;
				case "BirthDate":
					Collections.sort(Singleton.userPartner, new OrderBirthDate());
					break;
				}
			}
		}
	}
	public static void ChancheOrderAssociate() {
		String [] type = {"DNI", "Name", "Age", "BirthDate"};
		String main = "";
		
		if (Singleton.userAssociate.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Data is empty");
		}
		else {
			main = Menu.menuCombo(type, "How can you order the users?", "Lawyer");
			if(main != null) {
				switch(main) {
				case "DNI":
					Collections.sort(Singleton.userAssociate);
					break;
				case "Name":
					Collections.sort(Singleton.userAssociate, new OrderName());
					break;
				case "Age":
					Collections.sort(Singleton.userAssociate, new OrderAge());
					break;
				case "BirthDate":
					Collections.sort(Singleton.userAssociate, new OrderBirthDate());
					break;
				}
			}
		}
	}
	public static void ChancheOrderParalegal() {
		String [] type = {"DNI", "Name", "Age", "BirthDate"};
		String main = "";
		
		if (Singleton.userParalegal.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Data is empty");
		}
		else {
			main = Menu.menuCombo(type, "How can you order the users?", "Lawyer");
			if(main != null) {
				switch(main) {
				case "DNI":
					Collections.sort(Singleton.userParalegal);
					break;
				case "Name":
					Collections.sort(Singleton.userParalegal, new OrderName());
					break;
				case "Age":
					Collections.sort(Singleton.userParalegal, new OrderAge());
					break;
				case "BirthDate":
					Collections.sort(Singleton.userParalegal, new OrderBirthDate());
					break;
				}
			}
		}
	}
}
