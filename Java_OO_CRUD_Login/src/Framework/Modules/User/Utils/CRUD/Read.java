package Framework.Modules.User.Utils.CRUD;

import javax.swing.JOptionPane;

import Framework.Modules.User.Classes.Associate;
import Framework.Modules.User.Classes.Paralegal;
import Framework.Modules.User.Classes.Partner;
import Framework.Modules.User.Classes.Singleton.Singleton;
import Framework.Utils.Menu;

public class Read {
	public static void ReadPartner() {
		int main = 0, search = -1;
		String type[] =  {"All", "One"};
		Partner P = null;
		
		if(Singleton.userPartner.isEmpty()) {
			JOptionPane.showMessageDialog(null,"The user don't exist");
		}
		else {
			main = Menu.menuBotons(type, "All the users", "Lawyer");
			switch (main) {
				case 0:
					for (int i = 0; i < Singleton.userPartner.size();i++){
						String cad = "";
						cad = cad + (Singleton.userPartner.get(i).toString());
						JOptionPane.showMessageDialog(null, cad);
					}
					break;
				case 1:
					P = Find.IDPartner();
					if(P != null) {
						search = Find.FindPartner(P);
						if (search != -1) {
							System.out.println(search);
							P = Singleton.userPartner.get(search);
							JOptionPane.showMessageDialog(null, P.toString());
						}
						else {
							JOptionPane.showMessageDialog(null, "The user don't exist or the DNI is incorrect");
						}
					}
					break;
			}	
		}
	}
	public static void ReadAssociate() {
		int main = 0, search = -1;
		String type[] = {"All", "One"};
		Associate A = null;
		
		if(Singleton.userAssociate.isEmpty()) {
			JOptionPane.showMessageDialog(null, "The user don't exist");
		}
		else {
			main = Menu.menuBotons(type, "All the users", "Lawyer");
			switch(main) {
				case 0:
					for(int i = 0; i < Singleton.userAssociate.size();i++) {
						String cad = "";
						cad = cad + (Singleton.userAssociate.get(i).toString());
						JOptionPane.showMessageDialog(null, cad);
					}
					break;
				case 1:
					A = Find.IDAssociate();
					if(A != null) {
						search = Find.FindAssociate(A);
						if(search != -1) {
							A = Singleton.userAssociate.get(search);
							JOptionPane.showMessageDialog(null, A.toString());
						}
						else {
							JOptionPane.showMessageDialog(null, "The user don't exist or the DNI is incorrect");
						}
					}
					break;
			}
		}
	}
	public static void ReadParalegal() {
		int main = 0, search = -1;
		String type[] = {"All", "One"};
		Paralegal PA = null;
		
		if(Singleton.userParalegal.isEmpty()) {
			JOptionPane.showMessageDialog(null, "The user don't exist");
		}
		else {
			main = Menu.menuBotons(type, "All the users", "Lawyer");
			switch(main) {
			case 0: 
				for(int i = 0; i < Singleton.userParalegal.size(); i++) {
						String cad = "";
						cad = cad + (Singleton.userParalegal.get(i).toString());
						JOptionPane.showMessageDialog(null, cad);
				}
				break;
			case 1:
				PA = Find.IDParalegal();
				if(PA != null) {
					search = Find.FindParalegal(PA);
					if(search != -1) {
						PA = Singleton.userParalegal.get(search);
						JOptionPane.showMessageDialog(null, PA.toString());	
					}
					else {
						JOptionPane.showMessageDialog(null, "The user don't exist or the DNI is incorrect");
					}
				}
				break;
			}
		}
	}
}
