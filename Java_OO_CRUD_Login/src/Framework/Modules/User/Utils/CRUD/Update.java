package Framework.Modules.User.Utils.CRUD;

import javax.swing.JOptionPane;

import Framework.Modules.User.Classes.Associate;
import Framework.Modules.User.Classes.Paralegal;
import Framework.Modules.User.Classes.Partner;
import Framework.Modules.User.Classes.Singleton.Singleton;
import Framework.Modules.User.Utils.Functions_lawyer;

public class Update {

	public static void UpdatePartner() {
		int search = -1;
		Partner P = null;
		
		if(Singleton.userPartner.isEmpty()){
			JOptionPane.showMessageDialog(null, "The user is empty");
		}
		else{
			P = Find.IDPartner();
			if(P != null) {
				search = Find.FindPartner(P);
				if(search != -1) {
					P = Singleton.userPartner.get(search);
					Functions_lawyer.change_data(P);
					Singleton.userPartner.set(search, P);
				}
				else {
					JOptionPane.showMessageDialog(null, "Error");
				}
			}
		}
	}
	public static void UpdateAssociate() {
		int search = -1;
		Associate A = null;
		
		if(Singleton.userAssociate.isEmpty()) {
			JOptionPane.showMessageDialog(null, "The user is empty");
		}
		else {
			A = Find.IDAssociate();
			if(A != null) {
				search = Find.FindAssociate(A);
				if(search != -1) {
					A = Singleton.userAssociate.get(search);
					Functions_lawyer.change_data(A);
					Singleton.userAssociate.set(search, A);
				}
				else {
					JOptionPane.showMessageDialog(null, "Error");
				}
			}
		}
	}
	public static void UpdateParalegal() {
		int search = -1;
		Paralegal PA = null;
		
		if(Singleton.userParalegal.isEmpty()) {
			JOptionPane.showMessageDialog(null, "The user is empty");
		}
		else {
			PA = Find.IDParalegal();
			if (PA != null) {
				search = Find.FindParalegal(PA);
				if(search != -1) {
					PA = Singleton.userParalegal.get(search);
					Functions_lawyer.change_data(PA);
					Singleton.userParalegal.set(search, PA);
				}
				else {
					JOptionPane.showMessageDialog(null, "Error");
				}
			}
		}
	}
}
