package Framework.Modules.User.Utils;

import javax.swing.JOptionPane;
import Framework.Utils.*;

public class Data_lawyer {

	public static String validatename() {
		String name = "";
		boolean switx = false;
		
		while(switx != true) {
			name = Functions.validaString("Lawyer", "Name: ");
			switx = Validaters.validate_name(name);
			if(switx == false)
				JOptionPane.showMessageDialog(null, "Invalid name, try again please (First letter in mayus)");
		}
		return name;
	}
	public static String validatelastname() {
		String lastName = "";
		boolean switx = false;
		
		while(switx != true) {
			lastName = Functions.validaString("Lawyer", "Last name: ");
			switx = Validaters.validate_lastName(lastName);
			if(switx == false)
				JOptionPane.showMessageDialog(null, "Invalid last name, try again please (First letter in mayus)");
		}
		return lastName;
	}
	public static String validatedni() {
		String dni = "";
		boolean switx = false;
		
		while(switx != true) {
			dni = Functions.validaString("Lawyer", "DNI: ");
			switx = Validaters.validate_dni(dni);
			if(switx == false)
				JOptionPane.showMessageDialog(null, "Invalid DNI, try again please (The letter in mayus)");
		}
		return dni;
	}
	public static String validatetelephone() {
		String telephone = "";
		boolean switx = false;
		
		while(switx != true) {
			telephone = Functions.validaString("Lawyer", "Telephone:");
			switx = Validaters.validate_telephone(telephone);
			if(switx == false)
				JOptionPane.showMessageDialog(null, "Invalid telephone, try again please");
		}
		return telephone;
	}
	public static String validateemail() {
		String email = "";
		boolean switx = false;
		
		while(switx != true) {
			email = Functions.validaString("Lawyer", "Email:");
			switx = Validaters.validate_email(email);
			if(switx == false)
				JOptionPane.showMessageDialog(null, "Invalid email, try again please");
		}
		return email;
	}
	public static String validatepassword() {
		String passwd = "";
		boolean switx = false;
		
		while(switx != true) {
			passwd = Functions.validaString("Lawyer", "Password:");
			switx = Validaters.validate_password(passwd);
			if(switx == false)
				JOptionPane.showMessageDialog(null, "Invalid password, try again please");
		}
		return passwd;
	}
	public static int validateinvoicing() {
		int invoicing = 0;
		String invoicingS = "";
		boolean switx = false;
		
		while(switx != true) {
			invoicingS = Functions.validaString("Lawyer", "Invoicing:");
			switx = Validaters.validate_invoicing(invoicingS);
			if(switx == false)
				JOptionPane.showMessageDialog(null, "Invalid invoicing, try again please");
		}
		invoicing = Integer.parseInt(invoicingS);
		return invoicing;
	}
	public static int validateantiquity() {
		int antiquity = 0;
		String antiquityS = "";
		boolean switx = false;
		
		while(switx != true) {
			antiquityS = Functions.validaString("Lawyer", "Antiquity:");
			switx = Validaters.validate_antiquity(antiquityS);
			if(switx == false)
				JOptionPane.showMessageDialog(null, "Invalid data, try again please");
		}
		antiquity = Integer.parseInt(antiquityS);
		return antiquity;
	}
	public static int validatewin() {
		int win = 0;
		String winS = "";
		boolean switx = false;
		
		while(switx != true) {
			winS = Functions.validaString("Lawyer", "Number of win cases:");
			switx = Validaters.validate_win(winS);
			if(switx == false)
				JOptionPane.showMessageDialog(null, "Invalid data, try again please");
		}
		win = Integer.parseInt(winS);
		return win;
	}
	public static int validatelost() {
		int lost = 0;
		String lostS = "";
		boolean switx = false;
		
		while(switx != true) {
			lostS = Functions.validaString("Lawyer", "Number of lost cases:");
			switx = Validaters.validate_lost(lostS);
			if(switx == false)
				JOptionPane.showMessageDialog(null, "Invalid data, try again please");
		}
		lost = Integer.parseInt(lostS);
		return lost;
	}
	public static int validatebonuses() {
		int bonuses = 0;
		String bonusesS = "";
		boolean switx = false;
		
		while(switx != true) {
			bonusesS = Functions.validaString("Lawyer", "Bonuses:");
			switx = Validaters.validate_bonuses(bonusesS);
			if(switx == false)
				JOptionPane.showMessageDialog(null, "Invalid bonuses, try again please");
		}
		bonuses = Integer.parseInt(bonusesS);
		return bonuses;
	}
}
