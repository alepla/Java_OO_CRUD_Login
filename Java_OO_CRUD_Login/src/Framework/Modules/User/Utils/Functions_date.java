package Framework.Modules.User.Utils;

import javax.swing.JOptionPane;
import Framework.Classes.*;
import Framework.Utils.Functions;
import Framework.Utils.Validaters;
import Framework.Modules.User.Classes.*;

public class Functions_date {

	public static Dateps Date_BirthDate () {
		boolean switx = false;
		Dateps D = null;
		String BD = "";
		int age = 0;
		
		while(switx == false){
			BD = Functions.validaString("Lawyer", "Birth Date:");
			switx = Validaters.validate_date(BD);
			if (switx == false) {
				JOptionPane.showMessageDialog(null,"Invalid format, try again please");
			} 
			else {
				D = new Dateps(BD);
				switx = D.validateDate();
				if(switx == false) 
					JOptionPane.showMessageDialog(null, "Invalid date, please try again");
				else {
					age = D.RestSystemDate();
					if((age < 22) || (age > 99)) {
						switx = false;
						JOptionPane.showMessageDialog(null, "Invalid age");
					}
				}
			}
		}
		return D;
	}
	
	public static Dateps Years_working (Dateps date) {
		boolean switx = false;
		Dateps A = null;
		String BD = "";
		int compara = 0, antiquiti = 0, age = 0;
		
		while(switx == false){
			BD = Functions.validaString("Lawyer", "Date being working");
			switx = Validaters.validate_date(BD);
			if (switx == false) {
				JOptionPane.showMessageDialog(null,"Invalid format, try again please");
			} 
			else {
				A = new Dateps(BD);
				switx = A.validateDate();
				if(switx == false)
					JOptionPane.showMessageDialog(null, "Invalid date, please try again");
				else {
					compara = A.ComparateDate(date);
					if(compara != 1) {
						switx = false;
						JOptionPane.showMessageDialog(null, "This date is not real, you shouldn't be working");
					}
					else {
						age = A.RestTwoDates(date);
						System.out.println(age);
						if(age < 22) {
							switx = false;
							JOptionPane.showMessageDialog(null, "This date is not real, you shouldn't be working");
						}
						else {
							antiquiti = A.RestSystemDate();
							if((antiquiti <= 0) || (antiquiti >= 6)) {
								switx = false;
								JOptionPane.showMessageDialog(null, "Invalid date, you not be working how associate");
							}
						}
					}
				}
			}
		}
		return A;
	}
	
	public static Dateps Permanence_contract(Dateps date) {
		boolean switx = false, switx2 = false;
		Dateps P = null;
		String BD = "";
		int compare = 0, contract = 0, pin = 0, cont = 0, age = 0;
		
		while(switx == false) {
			BD = Functions.validaString("Lawyer", "Date being working");
			switx = Validaters.validate_date(BD);
			if(switx == false) {
				JOptionPane.showMessageDialog(null, "Invalid format, please try again");
			}
			else {
				P = new Dateps(BD);
				switx = P.validateDate();
				if(switx == false)
					JOptionPane.showMessageDialog(null, "Invalid date, please try again");
				else {
					compare = P.ComparateDate(date);
					if (compare != 1) {
						switx = false;
						JOptionPane.showMessageDialog(null, "This date is not real, you shouldn't be working");
					}
					else {
						age = P.RestTwoDates(date);
						System.out.println(age);
						if(age < 22) {
							switx = false;
							JOptionPane.showMessageDialog(null, "This date is not real, you shouldn't be working");
						}
						else {
							contract = P.RestSystemDate();
							if ((contract <= 0) || (contract > 3)) {
								switx = false;
								JOptionPane.showMessageDialog(null, "Invalid date, your contract has expired, \n"
																  + " you had 3 days to renew it, \n"
																  + " after you need itroduce the PIN that \n"
																  + " will be provided to you by the lawyer in charge of you.");
								while (switx2 == false) {
									pin = Functions.validaInt("Lawyer", "PIN");//1234 el pin para contunar
									switx2 = Validaters.validate_pin(Integer.toString(pin));
									if (switx2 == false) {
										JOptionPane.showMessageDialog(null, "Invalid format, please try again");
									} 
									else {
										if (pin != 1234) {
											switx2 = false;
											JOptionPane.showMessageDialog(null, "Invalid PIN, you have 3 attempts");
											cont++;
											if (cont == 3) {
												JOptionPane.showMessageDialog(null, "You have spent all the attemps");
												System.exit(0);
											}
										} 
										else {
											switx = true;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return P;
	}
	public static Dateps Change_date(Associate A, Paralegal P) {
		boolean switx = false;
		Dateps F = null;
		int age = 0, antiquiti = 0;
		
		while(switx == false) {
			F = Functions_date.Date_BirthDate();
			age = F.RestSystemDate();
			if(P == null) {
				antiquiti = ((Associate)A).getYears();
			}
			else if(A == null) {
				antiquiti = ((Paralegal)P).getContract();
			}
			switx = F.Change_data(age, antiquiti);
			if(switx == false) {
				JOptionPane.showMessageDialog(null, "The date is unreal");
			}
		}
		return F;
	}
}
