package Framework.Modules.User.Utils;

import Framework.Utils.Menu;
import javax.swing.JOptionPane;
import Framework.Modules.User.Classes.*;
import Framework.Classes.*;
import Framework.Modules.User.Classes.Singleton.*;

public class Functions_lawyer {
	
	public static Lawyer request_data(int i){		
		Lawyer a = null;
		String [] type = {"M", "F"};
		String [] type2 = {"UPV", "UV", "Oxford", "Harvard", "Columbia", "Pringstone", "other"};
		String [] grade = {"C", "B", "B+", "A", "A+", "A++"};

		if( i == 1 ){
			String name = Data_lawyer.validatename();
			String lastName = Data_lawyer.validatelastname();
			String dni = Singleton.dni;
			String tlf = Data_lawyer.validatetelephone();
			Dateps birthDate = Functions_date.Date_BirthDate();
			String gender = Menu.menuCombo(type, "Male or Female?", "Partner");
			String email = Data_lawyer.validateemail();
			String passwd = Data_lawyer.validatepassword();
			int salary = 0;
			int invoicing = Data_lawyer.validateinvoicing();
		
			a = new Partner(name, lastName, dni, tlf, birthDate, gender, email, passwd, salary, invoicing);
			JOptionPane.showMessageDialog(null, "The data of the partner has been collected","Congratulations", JOptionPane.INFORMATION_MESSAGE);

		}
		
		else if( i == 2 ){
			String name = Data_lawyer.validatename();
			String lastName = Data_lawyer.validatelastname();
			String dni = Singleton.dni;
			String tlf = Data_lawyer.validatetelephone();
			Dateps birthDate = Functions_date.Date_BirthDate();
			Dateps years_working = Functions_date.Years_working(birthDate);
			String gender = Menu.menuCombo(type, "Male of Female?", "Associate");
			String average_grade = Menu.menuCombo(grade, "Average grade", "Associate");
			int  win = Data_lawyer.validatewin();
			int lost = Data_lawyer.validatelost();
			int bonuses = Data_lawyer.validatebonuses();

			a = new Associate(name, lastName, dni, tlf, birthDate, average_grade, gender, years_working, win, lost, bonuses);
			JOptionPane.showMessageDialog(null, "The data of the associate has been collected","Congratulations", JOptionPane.INFORMATION_MESSAGE);

		}
		
		else if( i == 3 ){	
			String name = Data_lawyer.validatename();
			String lastName = Data_lawyer.validatelastname();
			String dni = Singleton.dni;
			String tlf = Data_lawyer.validatetelephone();
			Dateps birthDate = Functions_date.Date_BirthDate();
			String gender = Menu.menuCombo(type, "Male or female?", "Paralegal");
			String studies = Menu.menuCombo(type2, "University", "Paralegal");
			Dateps permanence_contract = Functions_date.Permanence_contract(birthDate);
			String email = Data_lawyer.validateemail();
			String passwd = Data_lawyer.validatepassword();
			
			a = new Paralegal(name, lastName, dni, tlf, birthDate, gender, studies, permanence_contract, email, passwd);
			JOptionPane.showMessageDialog(null, "The data of the Paralegal has been collected","Congratulations", JOptionPane.INFORMATION_MESSAGE);

		}	
		return a;
	}
	
	public static Partner RequestPartner() {
		Singleton.dni = Data_lawyer.validatedni();
		
		return new Partner(Singleton.dni);
	}
	public static Associate RequestAssociate() {
		Singleton.dni = Data_lawyer.validatedni();
		
		return new Associate(Singleton.dni);
	}
	public static Paralegal RequestParalegal() {
		Singleton.dni = Data_lawyer.validatedni();
		
		return new Paralegal(Singleton.dni);
	}
	
	public static void change_data(Lawyer a){
		String [] type = {"M", "F"};
		String [] type3 = {"UPV", "UV", "Oxford", "Harvard", "Columbia", "Pringstone", "other"};
		String [] grade = {"C", "B", "B+", "A", "A+", "A++"};
		
		if(a instanceof Partner){
			String [] type2 = {"Name", "Last name", "DNI", "Telephone", "Birth date", "Gender", "Email", "Passwd", "Invoicing"};
			String op1 = "";
			op1 = Menu.menuCombo(type2, "Chose the option to change", "Partner");
			
			switch(op1.toString()){
			
				case "Name":
					((Partner)a).setName(Data_lawyer.validatename());
					break;
				case "Last name":
					((Partner)a).setLastName(Data_lawyer.validatelastname());
					break;
				case "DNI": 
					((Partner)a).setDni(Data_lawyer.validatedni());
					break;
				case "Telephone": 
					((Partner)a).setTlf(Data_lawyer.validatedni());
					break;
				case "Birth date": 
					((Partner)a).setBirthDate(Functions_date.Date_BirthDate());
					break;
				case "Gender":
					((Partner)a).setGender(Menu.menuCombo(type, "Male or female?", "Associate "));
					break;
				case "Email": 
					((Partner)a).setEmail(Data_lawyer.validateemail());
					break;
				case "Passwd": 
					((Partner)a).setPasswd(Data_lawyer.validatepassword());
					break;
				case "Invoicing": 
					((Partner)a).setInvoicing(Data_lawyer.validateinvoicing());
					break;
			}
		}
		if(a instanceof Associate){
			String [] type2 = {"Name", "Last name", "DNI", "Telephone", "Birth date", "Years Working", "Gender", "Average grade","Number of won cases", "Number of lost cases", "Bonuses"};
			String op2 = "";
			op2 = Menu.menuCombo(type2, "Chose the option to change", "Associate");
			
			switch(op2.toString()){
			
				case "Name":
					((Associate)a).setName(Data_lawyer.validatename());
					break;
				case "Last name":
					((Associate)a).setLastName(Data_lawyer.validatelastname());
					break;
				case "DNI": 
					((Associate)a).setDni(Data_lawyer.validatedni());
					break;
				case "Telephone": 
					((Associate)a).setTlf(Data_lawyer.validatetelephone());
					break;
				case "Birth date": 
					((Associate)a).setBirthDate(Functions_date.Change_date(((Associate)a), null));
					break;
				case "Years Working":
					Dateps birthDate = Functions_date.Years_working(((Associate) a).getBirthDate());
					((Associate) a).setYears_working(birthDate);
					break;
				case "Gender": 
					((Associate)a).setGender(Menu.menuCombo(type, "Male or female?", "Associate "));
					break;
				case "Average grade": 
					((Associate)a).setAverage_grade(Menu.menuCombo(grade, "Average grade", "Associate"));
					break;
				case "Number of won cases": 
					((Associate)a).setWin(Data_lawyer.validatewin());
					break;
				case "Number of lost cases": 
					((Associate)a).setLost(Data_lawyer.validatelost());
					break;
				case "Bonuses": 
					((Associate)a).setBonuses(Data_lawyer.validatebonuses());
					break;
			}
		}
		if(a instanceof Paralegal){
			String [] type2 = {"Name", "Last name", "DNI", "Telephone", "Birth date", "Gender", "University","Permanence contract", "Email","Password"};
			String op3 = "";
			op3 = Menu.menuCombo(type2, "Chose the option to change", "Associate");
			
			switch(op3.toString()){
			
				case "Name":
					((Paralegal)a).setName(Data_lawyer.validatename());
					break;
				case "Last name":
					((Paralegal)a).setLastName(Data_lawyer.validatelastname());
					break;
				case "DNI": 
					((Paralegal)a).setDni(Data_lawyer.validatedni());
					break;
				case "Telephone": 
					((Paralegal)a).setTlf(Data_lawyer.validatetelephone());
					break;
				case "Birth date": 
					((Paralegal)a).setBirthDate(Functions_date.Change_date(null, (Paralegal)a));
					break;
				case "Gender": 
					((Paralegal)a).setGender(Menu.menuCombo(type, "Male or female?", "Associate "));
					break;
				case "Univerity": 
					((Paralegal)a).setStudies(Menu.menuCombo(type3, "University", "Paralegal"));
					break;
				case "Permanence contract":
					Dateps birthDate = Functions_date.Permanence_contract(((Paralegal) a).getBirthDate());
					((Paralegal) a).setPermanence_contract(birthDate);
					break;
				case "Email": 
					((Paralegal)a).setEmail(Data_lawyer.validateemail());
					break;
				case "Password": 
					((Paralegal)a).setPasswd(Data_lawyer.validatepassword());
					break;
			}
		}
	}
	public static String read(Lawyer a) {
		String cad = "";
		if (a instanceof Partner) {
			cad = ((Partner) a).toString();
		}
		if (a instanceof Associate) {
			cad = ((Associate) a).toString();
		}
		if (a instanceof Paralegal) {
			cad = ((Paralegal) a).toString();
		}
		return cad;
	}
	public static Lawyer delete(Lawyer a) {
		a = null;
		return a;
	}
}
