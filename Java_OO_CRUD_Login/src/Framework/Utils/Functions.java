package Framework.Utils;

import javax.swing.JOptionPane;

public class Functions {

	//Se pide un numero int
		public static int validaInt(String titulo, String enunciado) {
			int a = 0;
			String s = "";
			boolean correcto=true;
				
			do{
				try{
					s=JOptionPane.showInputDialog(null, enunciado,titulo,JOptionPane.QUESTION_MESSAGE);
					if(s==null){
						JOptionPane.showMessageDialog(null, "Exiting of the aplication","Exiting",JOptionPane.INFORMATION_MESSAGE);
						System.exit(0);//al usuario pulsar cancelar o cerrar la vtna del showinputdialog, acaba la ejecucion
					}else {
						a=Integer.parseInt(s);
						correcto=true;
					}
				}catch(Exception e){
					JOptionPane.showMessageDialog(null, "You did not enter a number", "Error",JOptionPane.ERROR_MESSAGE);
					correcto=false;
				}
			}while(correcto==false);
			return a;
		}
		//Se pide un numero float
		public static float validaFloat(String s){
			float b = 0.0f;
			boolean correcto=true;

			do{
				try{
					s=JOptionPane.showInputDialog(null, "Write a float number","Float number",JOptionPane.QUESTION_MESSAGE);
					if(s==null){
						JOptionPane.showMessageDialog(null, "Exiting of the aplication","Exiting",JOptionPane.INFORMATION_MESSAGE);
						System.exit(0);//al usuario pulsar cancelar o cerrar la vtna del showinputdialog, acaba la ejecucion
					}else {
						b=Float.parseFloat(s);
						correcto=true;
					}
				}catch(Exception e){
					JOptionPane.showMessageDialog(null, "You not entered a float number", "Error",JOptionPane.ERROR_MESSAGE);
					correcto=false;
				}
			}while(correcto==false);
			return b;
		}
		//Se pide un caracter
		public static char validaChar(String s) {
			char c = 0;
			boolean correcto=true;

			do{
				try{
					s=JOptionPane.showInputDialog(null, "Write a char","Char",JOptionPane.QUESTION_MESSAGE);
					if(s==null){
						JOptionPane.showMessageDialog(null, "Exiting of the aplication","Exiting",JOptionPane.INFORMATION_MESSAGE);
						System.exit(0);//al usuario pulsar cancelar o cerrar la vtna del showinputdialog, acaba la ejecucion
					}else {
						c=s.charAt(0);
						correcto=true;
					}
				}catch(Exception e){
					JOptionPane.showMessageDialog(null, "You not entered a char", "Error",JOptionPane.ERROR_MESSAGE);
					correcto=false;
				}
			}while(correcto==false);
			return c;
		}
		//se pide una cadena
		public static String validaString(String titulo, String enunciado ) {
			boolean correcto=true;
			String s = "";

			do{
				try{
					s=JOptionPane.showInputDialog(null, enunciado,titulo,JOptionPane.QUESTION_MESSAGE);
					correcto=true;
					if (s==null){
						JOptionPane.showMessageDialog(null, "Exiting of the aplication","Exiting",JOptionPane.INFORMATION_MESSAGE);
						System.exit(0);//al usuario pulsar cancelar o cerrar la vtna del showinputdialog, acaba la ejecucion
					}
					if(s.equals("")){
						JOptionPane.showMessageDialog(null, "Entry error","Error",JOptionPane.ERROR_MESSAGE);
						correcto=false;
					}
				}catch(Exception e){
					JOptionPane.showMessageDialog(null, "You have not entered a string", "Error",JOptionPane.ERROR_MESSAGE);
					correcto=false;
				}
			}while(correcto==false);
			return s;
		}
}
