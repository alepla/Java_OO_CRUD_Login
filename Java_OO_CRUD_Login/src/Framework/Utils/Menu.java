package Framework.Utils;

import javax.swing.JOptionPane;

public class Menu {

	//MENU COMBO
	public static String menuCombo(String[] options, String message, String title){
		Object option = null;
		option = JOptionPane.showInputDialog(null, message, title, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

		/*if(option == null) {
			JOptionPane.showMessageDialog(null,"Exiting of the aplication","Exiting ",JOptionPane.INFORMATION_MESSAGE);
			System.exit(0);
		}*/
		if(option != null) {
			return option.toString();
		}
		return null;
	}


	//MENU BOTONS 
	public static int menuBotons(String[] options, String message, String title){
		int opcion = 0;
		
		opcion = JOptionPane.showOptionDialog(null, message, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,null, options, options[0]);
			
		if(opcion == -1) {
			JOptionPane.showMessageDialog(null,"Exiting of the aplication","Exiting",JOptionPane.INFORMATION_MESSAGE);
			System.exit(0);
		}

		return opcion;
	}
}