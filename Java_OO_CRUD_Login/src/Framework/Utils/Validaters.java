package Framework.Utils;

import java.util.regex.*;

public class Validaters {
	
	public static boolean validate_email(String email) {
		 Pattern pat = Pattern.compile("[^@]+@[^@]+\\.[a-zA-Z]{2,}");
		 Matcher mat = pat.matcher(email);
	     if (mat.matches() == true) 
	    	 return true;
	     else 
	      	return false;
	     
	}
	public static boolean validate_name(String name) {
		Pattern pat = Pattern.compile("^([A-Z]{1}[a-zA-Z]*)$");
		Matcher mat = pat.matcher(name);
		if(mat.matches() == true)
			return true;
		else
			return false;
	}
	public static boolean validate_lastName(String lastName) {
		Pattern pat = Pattern.compile("^([A-Z]{1}[a-zA-Z]*)$");
		Matcher mat = pat.matcher(lastName);
		if(mat.matches() == true)
			return true;
		else
			return false;
	}
	public static boolean validate_telephone(String telephone) {
		Pattern pat = Pattern.compile("^[0-9]{9}$");
		Matcher mat = pat.matcher(telephone);
		if(mat.matches() == true)
			return true;
		else
			return false;
	}
	public static boolean validate_dni(String dni) {
		Pattern pat = Pattern.compile("^([0-9]{8}[A-Z])$");
		Matcher mat = pat.matcher(dni);
		if(mat.matches() == true)
			return true;
		else
			return false;	
	}
	public static boolean validate_password(String passwd) {
		Pattern pat = Pattern.compile("^[a-zA-Z0-9$@$!%*?&]*$");
		Matcher mat = pat.matcher(passwd);
		if(mat.matches() == true)
			return true;
		else
			return false;
	}
	public static boolean validate_date(String date) {
		Pattern pat = Pattern.compile("^[0-9]{2}[/][0-9]{2}[/][0-9]{4}$");
		Matcher mat = pat.matcher(date);
		if(mat.matches() == true)
			return true;
		else
			return false;
	}
	public static boolean validate_invoicing(String invoicing) {
		Pattern pat = Pattern.compile("^[0-9]{1,5}$");
		Matcher mat = pat.matcher(invoicing);
		if(mat.matches() == true)
			return true;
		else
			return false;
	}
	public static boolean validate_antiquity(String antiquity) {
		Pattern pat = Pattern.compile("^[0-9]{1,2}$");
		Matcher mat = pat.matcher(antiquity);
		if(mat.matches() == true)
			return true;
		else
			return false;
	}

	public static boolean validate_win(String win) {
		Pattern pat = Pattern.compile("^[0-9]{1,3}$");
		Matcher mat = pat.matcher(win);
		if(mat.matches() == true)
			return true;
		else
			return false;
	}
	public static boolean validate_lost(String lost) {
		Pattern pat = Pattern.compile("^[0-9]{1,3}$");
		Matcher mat = pat.matcher(lost);
		if(mat.matches() == true)
			return true;
		else
			return false;
	}
	public static boolean validate_bonuses(String bonuses) {
		Pattern pat = Pattern.compile("^[0-9]{1,5}$");
		Matcher mat = pat.matcher(bonuses);
		if(mat.matches() == true)
			return true;
		else
			return false;
	}
	public static boolean validate_pin(String pin) {
		Pattern pat = Pattern.compile("^[0-9]{1,4}$");
		Matcher mat = pat.matcher(pin);
		if(mat.matches() == true)
			return true;
		else
			return false;
	}
	
	
}
